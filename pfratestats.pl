#!/usr/bin/perl
#
# pfratestats.pl - get statistics about mail rate from postfix logs
# Copyright (c) 2023 - 2024 by Matus UHLAR <uhlar@fantomas.sk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package pfratestats;

use Getopt::Long;

GetOptions('d+', 'h', 'p+');

if ($opt_d>=1) {
	open STDERR, ">&STDOUT";
	STDERR->autoflush(1);
	STDOUT->autoflush(1);
}

if ($opt_h) {
	print STDERR
"pfratestats.pl Copyright (c) 2023-2024 Matus UHLAR - fantomas
Program comes with NO WARRANTY.
You can redistribute it under terms of GPL version 3 or any later version.

Usage:
	$0 [ -h ] [ -d ] [ input ] [...]
		-h	this help
		-d	increase debug level
		-p	print mail info
";

	exit 0;
}

sub printmailinfo (%) {
	my $mail = shift @_;
	print $mail->{'time'}, "\t";
	print $mail->{'queueid'},"\t";
	print $mail->{'client'}, "\t";
	print $mail->{'size'}, "\t";
	print $mail->{'nrcpt'}, "\t";
	print $mail->{'from'}, "\t";
	print $mail->{'user'}, "\n";
}

my %mail;

while (<>) {
	if ($opt_d>=1) {
		print "input\n\t", $_;
	}
	chomp;
	my ($mon, $day, $time, $host, $fullprg, $log) = split /\s+/, $_, 6;
	if ($mon eq "") {
		if ($opt_d>=1) {
			print STDERR "Continued line ignored\n";
		}
		next;
	}
	if (!defined $log) {
		print STDERR "Input parse failed step 1\n\t", $_;
		next;
	}
	my $fulltime = "$mon $day $time";
	if ($opt_d>=2) {
		print "step 1\n\ttime\t\"$fulltime\"\n\thost\t\"$host\"\n\tfullprg\t\"$fullprg\"\n\tlog\t\"$log\"\n";
	}

	my ($inst)=substr($fullprg,0,index($fullprg,'/'));
	if ($inst ne "postfix") {
		if ($opt_d>=1) {
			print "not wanted postfix instance, skipping\n";
		}
		next;
	}

	my ($prog)=substr($fullprg,rindex($fullprg,'/')+1);
	substr($prog,rindex($prog,'['))='';
	if ($opt_d>=2) {
		print "step 2\n\tinst\t$inst\n\tprog\t$prog\n";
	}

	if (
		   $prog eq "local" || $prog eq "smtp"
		|| $prog eq "postscreen" || $prog eq "dnsblog"
		|| $prog eq "tlsproxy" || $prog eq "anvil"
		|| $prog eq "lmtp" || $prog eq "scache"
		|| $prog eq "bounce" || $prog eq "pickup"
		|| $prog eq "cleanup" || $prog eq "error"
		|| $prog eq "verify"
		) {
		if ($opt_d>=1) {
			print "not wanted prog, skipping\n";
		}
		next;
	}

	my @logline = split / /, $log, 5;
	if ($opt_d>=2) {
		print "step3\n\t0\t$logline[0]\n\t1\t$logline[1]\n\t2\t$logline[2]\n\t3\t$logline[3]\n\t4\t$logline[4]\n";
	}
	# parse queue ID now because multiple types of logs need it
	my $queueid = shift @logline;
	$queueid =~ tr/://d;
	if ($opt_d>=2) {
		print "step4\n\tqueueid\t$queueid\n";
	}

	if ($prog eq "smtpd") {
		if ($queueid eq "connect" || $queueid eq "disconnect"
		 || $queueid eq "NOQUEUE" || $queueid eq "warning"
		 || $queueid eq "lost" || $queueid eq "timeout"
		 || $queueid eq "SSL_accept" || $queueid eq "improper"
		) {
			if ($opt_d>=1) {
				print "ignoring $prog $queueid\n";
			}
			next;
		}
		my $first = shift @logline;
		if ($first eq "reject:") {
			if ($opt_d>=1) {
				print "ignoring $prog line\n";
			}
			next;
		}
		$mail{$queueid}{'queueid'}=$queueid;
		$mail{$queueid}{'time'}=$fulltime;
		if ($first =~ m/^client=/ ) {
			# extract client IP and username
			my ($client) = substr($first,index($first,'[')+1);
			substr($client,index($client,']'))='';
			if ($opt_d>=2) {
				print "\tclient\t$client\n";
			}
			$mail{$queueid}{'client'}=$client;
			$first = shift @logline;
			if (defined $first) {
					if ($first =~ m/^sasl_method=/ && ($first = shift @logline) =~ m/^sasl_username=/) {
					my $user=substr($first , index($first , '=')+1);
					if ($opt_d>=2) {
						print "\tuser\t$user\n";
					}
					$mail{$queueid}{'user'}=$user;
				} else {
					print STDERR "unknown $prog line\n";
				}
			}
			next;
		}
		print STDERR "unknown $prog line\n";
		next;
	}

	if ($prog eq "qmgr") {
		my $first = shift @logline;
		if ($first eq "removed") {
			if ($opt_d>=1) {
				print "ignoring $prog line\n";
			}
			next;
		}
		if (!exists $mail{$queueid}) {
			# we have not seen smtpd line with source of this mail
			if ($opt_d>=1) {
				print "queueid $queueid source unknown\n";
			}
			next;
		}
		if (exists $mail{$queueid}{'from'}) {
			if ($opt_d>=1) {
				print "queueid $queueid already processed\n";
			}
			next;
		}
		if ($first =~ m/^from=</) {
			my $from = substr($first, index($first,'<')+1);
			substr($from,index($from,'>'))='';
			if ($from eq "") { $from='<>'; }
			if ($opt_d>=2) {
				print "\tfrom\t$from\n";
			}
			$mail{$queueid}{'from'}=$from;
			$first = shift @logline;
			if ($first =~ m/^size=(\d+),$/) {
				my $size = $1;
				if ($opt_d>=2) {
					print "\tsize\t$size\n";
				}
				$mail{$queueid}{'size'}=$size;
				$first = shift @logline;
				if ($first =~ m/nrcpt=(\d+)$/) {
					my $nrcpt = $1;
					if ($opt_d>=2) {
						print "\tnrcpt\t$nrcpt\n";
					}
					$mail{$queueid}{'nrcpt'}=$nrcpt;
					if ($opt_p>=1) {
						printmailinfo($mail{$queueid});
					}
					next;
				}
			}
		}
		print STDERR "unknown $prog line\n";
		next;
	}

	print STDERR "unknown prog $prog\n";
}
