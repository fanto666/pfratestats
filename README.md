## Name
pfratestats

## Description
Process postfix log files to see how many e-mails were sent by which sasl-authenticated users, from which IP adresses, to how many recipients, and how big they were.

## Badges

## Visuals

## Installation

## Usage

perl pfratestats.pl [-d] [-p] [file [....]]
- if no file given, stdin is used (perl iterator <>)

## Support

## Roadmap

- for each processed e-mail, print sasl user, sending IP, size, number of recipients

- parse date to timestamp format. Autodetect year and DST

- process statistics for given time
  - number of e-mails
  - total size of e-mails
  - number of recipients

## Contributing

## Authors and acknowledgment

## License
This package is licensed under GNU GPL version 3 or any later version.

## Project status
Project just starting.
